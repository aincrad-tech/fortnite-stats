import React from "react";
import Home from "src/views/Home";
import Player from "./views/Player";
import { Switch, Route, BrowserRouter as Router, } from "react-router-dom";



const App: React.FC = () => {
  return (
    <Router>
      <Switch>

        <Route path="/player/:username">
          <Player />
        </Route>

        <Route path="/">
          <Home />
        </Route>

      </Switch>
    </Router>
  );
};

export default App;
