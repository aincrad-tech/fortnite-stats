import React from "react";
import logo from "src/components/FortniteLogo.png";
import { Link } from "react-router-dom";
import S from "src/styles/HeaderForPlayers.module.scss"


interface Props {
    username: string
}

const HeaderForPlayer: React.FC<Props> = ({username}) => {

  return(
    <div className={S.container}>
      <Link to={"/"}>
        <img src={logo}/>
      </Link>
      <div className={S.username}>
        {username}
      </div>
    </div>
  )

}

export default HeaderForPlayer