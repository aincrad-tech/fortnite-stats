import React from "react";
import S from "src/styles/Player/MatchSelector.module.scss";

interface Props {
  type: string
}

const MatchSelector: React.FC<Props> = ({type}) => {
  return(
    <div className={S.container}>  
      <span>{type}</span>
    </div>    
  )
}

export default MatchSelector