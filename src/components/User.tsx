import React from "react";
import { Link } from "react-router-dom";
import S from "../styles/User.module.scss"

interface Props {
  username: string
}

const User: React.FC<Props> = ({username}) => { 

  return (
    <div className={S.userElement}>
      <Link to={"player/" + username} >
        <span className={S.name}>{username}</span>
      </Link>
    </div>
  );
};

export default User;
