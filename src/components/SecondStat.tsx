import React from "react";
import S from "src/styles/Player/SecondStat.module.scss"

interface Props{
  killsPerMatch: number
  minutesplayed: number
  placetop10: number
  placetop25: number
  score: number
  winrate: number
}



const SecondStat: React.FC<Props> = ({killsPerMatch,minutesplayed,placetop10,placetop25,score,winrate})=> {
  
  return(
    <div className={S.container}>
      <div className={S.stat}>
        {killsPerMatch} kills per match
      </div>
      <div className={S.stat}>
        {minutesplayed} minutes played
      </div>
      <div className={S.stat}>
        {winrate} winrate
      </div>
      <div className={S.stat}>
        {placetop10} match in top 10
      </div>
      <div className={S.stat}>
        {placetop25} match in top 25
      </div>

      <div className={S.score}>Score: {score}</div>     
    </div>
  )
}

export default SecondStat


/**/