import React from "react";
import S from "src/styles/Player/FirstStat.module.scss";

interface Props {
    type: string
    matchesPlayed:number
    kill: number
    win: number
  }

const FirstStat: React.FC<Props> = ({type, matchesPlayed,  kill,  win}) => {
  return(
    <div className={S.FirstStatContainer}>
      <div className={S.FirstStatMode}>{type}</div>
      <div className={S.FirstStatKill}>{kill}</div>
      <div className={S.FirstStatWin}>{win}</div>      
      <div className={S.FirstStatMatchesPlayed}>{matchesPlayed}</div>
    </div>
  )
}

export default FirstStat