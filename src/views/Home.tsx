import React from "react";
import S from "src/styles/Home.module.scss";
import User from "src/components/User";

import logo from "src/components/FortniteLogo.png";

class Home extends React.Component {
  state = {
    players: [
      {id: 1, userName: 'Fuoco4444'},
      {id: 2, userName: 'Placanica99'},
      {id: 3, userName: 'therealnezzy813'},
      {id: 4, userName: 'Ninja'},
      {id: 5, userName: 'LU-LA-96'},
      {id: 6, userName: 'Sgabu189718'}
    ]
  }
  
  render(){
    return (
      <div className={S.app} >
        <img src={logo}/>
        <div className={S.playerButtons} >
          {this.state.players.map((players) =>(
            <User username={players.userName} key={players.id}/>)
          )}
        </div>
      </div>
    );
  }
}

export default Home;
