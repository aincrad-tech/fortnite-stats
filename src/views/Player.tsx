import React, { useEffect, useState } from 'react'
import axios from 'axios';
import HeaderForPlayer from '../components/HeaderForPlayers';
import S from "src/styles/Player.module.scss";
import { useParams } from 'react-router';
import MatchSelector from 'src/components/MatchSelector';
import FirstStat from 'src/components/FirstStat';
import SecondStat from 'src/components/SecondStat';


const Player = () => {

  const {username}:{username:string} = useParams();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [data, setData] = useState<any>()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const body = async () => {
      const res = await axios.get("https://fortnite-api.p.rapidapi.com/stats/" + username,{
        headers:{
          "X-RapidAPI-Key": "ee6468bca6msh080f5fa6c47f0e6p1df26ajsnf732bcc5ba54"
        }
      })
      setData(res.data);
      setLoading(false)  
        
    }    
    
    body();
  },
  [username])

  console.log(data)

  return (
    <div className={S.app} >
      
      <div className={S.header} >
        <HeaderForPlayer username={username} />
      </div>
      
      <div className={S.modeContainer}>
        {loading && <div className={S.loading}>
          <div></div>
          <div></div>
          <div></div>
        </div>
        }
        {!loading && (
          <div className={S.container}>
            <div>
              <MatchSelector type="Singolo" />
              <MatchSelector type="Coppie" />
              <MatchSelector type="Terzetti" />
              <MatchSelector type="Squadre" />
              <MatchSelector type="Creativa" />
            </div>
            <div>
              <FirstStat 
                type="Singolo" 
                matchesPlayed={data.lifetime.all.defaultsolo.matchesplayed}
                kill={data.lifetime.all.defaultsolo.kills}
                win={data.lifetime.all.defaultsolo.placetop1}/>
            </div>
            <div>
              <SecondStat 
                killsPerMatch={data.lifetime.all.defaultsolo.killsPerMatch}
                minutesplayed={data.lifetime.all.defaultsolo.minutesplayed}
                placetop10={data.lifetime.all.defaultsolo.placetop10}
                placetop25={data.lifetime.all.defaultsolo.placetop25}
                score={data.lifetime.all.defaultsolo.score}
                winrate={data.lifetime.all.defaultsolo.winrate}
              />
            </div>
          </div>
        )}
      </div>                                                    
    </div>
  )
}

export default Player


/**/